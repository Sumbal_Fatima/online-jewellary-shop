<!doctype html>
<html>
	<head>
		<script src="js/jquery-1.9.0.js"></script>
		<link rel="stylesheet" href="style.css"/>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css"/>
	    <link href="css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="css/mdb.min.css" rel="stylesheet"/>
	    <link href="css/style.css" rel="stylesheet"/>
	</head>
	<body class="s">
		<form method="POST">		
			<div class="card">
			    <div class="card-block">
			        <div class="form-header  bg-primary" id="ss">
			            <h3 style="color:white;"><i class="fa fa-lock" style="color:white;"></i> Login:</h3>
			        </div>
			        </br>
			        </br>
			        <div class="md-form">
			            <i class="fa fa-envelope prefix"></i>
			            <input type="text" name="email" id="email" class="form-control" placeholder="Your Email"/>
			        </div>
			        <div class="md-form">
			            <i class="fa fa-lock prefix"></i>
			            <input type="password" name="password" id="password" class="form-control" placeholder="Your Password"/>
			        </div>
			        <div class="text-xs-center">
			            <button type="submit" class="btn btn-primary"  name="submit">Login</button>
			        </div>
				    <div class="modal-footer">
				        <div class="options">
				            <p>Not a member? <a href="SignUp.php">Sign Up</a></p>
				        </div>
				    </div>
				</div>
			</div>
		</form>
		<div id="result"></div>
	</body>
</html>
<?php
 	session_start();
	include("db.php");
	if(isset($_POST['submit'])){


		$server = "localhost";
		$dbname = "admin";
		$dsn = "mysql:host=$server;dbname=$dbname";
		$username = "root";
		$password = "";

		$con = new PDO($dsn,$username,$password);
		$con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
		$email=$_POST['email'];
		$pass=$_POST['password'];

		$statement = $con->prepare("SELECT *  FROM login WHERE Email=:email AND Password=:password");
		$statement->bindParam(':email', $email);
		$statement->bindParam(':password', $pass);
		
		$statement->execute();
		//$record = $statement->fetch(PDO::FETCH_ASSOC);

		if($statement->fetchColumn()==0){
			echo "Invalid Address";
		}
		else{
			 $_SESSION['email']=$email;
			header("location:admin.php?status=success");
		}
		
    }
    
?>