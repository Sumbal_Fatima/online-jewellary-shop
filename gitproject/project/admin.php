<!doctype html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link href="promdb/css/bootstrappro.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="promdb/css/mdbpro.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
        
	</head>
	<body>
		<?php
			include("db.php");

			session_start();

			if(!isset($_SESSION["email"])){
				header("location:login.php?error=1");
			}
			else{
				if(isset($_SESSION["email"])){

				}
			}

		?>
		<!--Navbar-->
		<nav class="navbar navbar-dark" style="background-color:#4B515D;">
            <!--Collapse button-->
		    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2">
		        <i class="fa fa-bars"></i>
		    </button> 
            <div class="container">
                <!--Collapse content-->
		        <div class="collapse navbar-toggleable-xs" id="collapseEx2">
		            <!--Navbar Brand-->
		            <a href="index.php" class="navbar-brand" style="color:white;">Home</a>
		            <!--Links-->
		            <ul class="nav navbar-nav">
		                <li class="nav-item">
		                    <a href="admin_customer.php" class="nav-link">Customer Details</a>
		                </li>
		                <li class="nav-item">
		                    <a href="admin_product.php" class="nav-link">Product Details</a>
		                </li>
		            </ul>

		            <form style="padding-top:8px; padding-left:12px; size:13px;" class="form-inline float-lg-right">
                        <a href="logout.php" class="nav-link">Logout</i></a>
		            </form>

		            <!--Search form-->
		            <form style="padding-left:8px;" class="form-inline float-lg-right">
		                <input class="form-control" type="text" placeholder="Search">
		            </form>
		        </div>
		        <!--/.Collapse content-->
		    </div>
		</nav>
		<!--/.Navbar-->
	</body>
</html>