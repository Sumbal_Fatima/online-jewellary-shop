<!doctype html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link href="promdb/css/bootstrappro.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="promdb/css/mdbpro.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
        
	</head>
	<body>

		<!--Navbar-->
		<nav class="navbar navbar-dark" style="background-color:#4B515D;">
            <!--Collapse button-->
		    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2">
		        <i class="fa fa-bars"></i>
		    </button> 
            <div class="container">
                <!--Collapse content-->
		        <div class="collapse navbar-toggleable-xs" id="collapseEx2">
		            <!--Navbar Brand-->
		            <a href="index.php" class="navbar-brand" style="color:white;">Home</a>
		            <!--Links-->
		            <ul class="nav navbar-nav">
		                <li class="nav-item">
		                    <a href="admin_customer.php" class="nav-link">Customer Details</a>
		                </li>
		                <li class="nav-item">
		                    <a href="admin_product.php" class="nav-link">Product Details</a>
		                </li>
		            </ul>

		            <form style="padding-top:8px; padding-left:12px; size:13px;" class="form-inline float-lg-right">
                        <a href="login.php" class="nav-link"><i class="fa fa-user" aria-hidden="true">Admin</i></a>
		            </form>

		            <!--Search form-->
		            <form style="padding-left:8px;" class="form-inline float-lg-right">
		                <input class="form-control" type="text" placeholder="Search">
		            </form>
		        </div>
		        <!--/.Collapse content-->
		    </div>
		</nav>
		<!--/.Navbar-->

		
		<!--Product table-->
		<div class="table-responsive">
		    <table class="table product-table">
		        <!--Table head-->
		        <thead>
		            <tr>
		                <th></th>
		                <th>Product</th>
		                <th>Color</th>
		                <th>Size</th>
		                <th>Price</th>
		                <th>QTY</th>
		                <th>Amount</th>
		                <th></th>
		            </tr>
		        </thead>
		        <!--/Table head-->

		        <!--Table body-->
		        <tbody>

		            <!--First row-->
		            <tr>
		                <th scope="row">
		                    <img src="http://mdbootstrap.com/images/ecommerce/products/shoes.jpg" alt="" class="img-fluid">
		                </th>
		             
		                <td>Blue</td>
		                <td>38</td>
		                <td>200 $</td>
		                <td>
		                    <span class="qty">1 </span>
		                   
		                </td>
		                <td>200 $</td>
		                <td>
		                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Remove item">X
		                    </button>
		                </td>
		            </tr>
		            <!--/First row-->

		            <!--Second row-->
		            <tr>
		                <th scope="row">
		                    <img src="http://mdbootstrap.com/images/ecommerce/products/sweater.jpg" alt="" class="img-fluid">
		                </th>
		                
		                <td>Blue</td>
		                <td>L</td>
		                <td>200 $</td>
		                <td>
		                    <span class="qty">3 </span>
		                </td>
		                <td>600 $</td>
		                <td>
		                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Remove item">X
		                    </button>
		                </td>
		            </tr>
		            <!--/Second row-->

		            <!--Third row-->
		            <tr>
		                <th scope="row">
		                    <img src="http://mdbootstrap.com/images/ecommerce/products/tie.jpg" alt="" class="img-fluid">
		                </th>
		             
		                <td>Blue</td>
		                <td>M</td>
		                <td>200 $</td>
		                <td>
		                    <span class="qty">2 </span>
		                </td>
		                <td>400 $</td>
		                <td>
		                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Remove item">X
		                    </button>
		                </td>
		            </tr>
		            <!--/Third row-->
		        </tbody>
		        <!--/Table body-->
		    </table>
		</div>
		<!--/Product table-->
        <!-- JQuery -->
        <script type="text/javascript" src="promdb/js/jquery3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="promdb/js/teether.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="promdb/js/boootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="promdb/js/mddb.min.js"></script>
	</body>
</html>	