


function validateForm(){

	var name = document.getElementById('name').value;
	var contact = document.getElementById('contact').value;
	var email = document.getElementById('email').value;

	var nameRegex = /^[a-zA-Z]+$/;
	var contactRegex = /^[0-9]{9}$/;
	var emailRegex=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	var flag = false;
	
	if(nameRegex.test(name) == false){
		document.getElementById('error_name').style.display='inline';
		flag = true;
	}
			
	if(contactRegex.test(contact) == false){
		document.getElementById('error_contact').style.display='inline';
		flag = true;
	}
				
	if(emailRegex.test(email) == false){
		document.getElementById('error_email').style.display='inline';
		flag3 = true;
	}

	if(flag == true)
		return false;
	}