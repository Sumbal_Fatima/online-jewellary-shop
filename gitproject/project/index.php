<!doctype html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link href="promdb/css/bootstrappro.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="promdb/css/mdbpro.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
        
	</head>
	<body>

		<div>
			<img src="I/banner2.jpg" class="img-fluid"/>
		</div>

		<!--Navbar-->
		<nav class="navbar navbar-dark" style="background-color:#4B515D;">
            <!--Collapse button-->
		    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2">
		        <i class="fa fa-bars"></i>
		    </button> 
            <div class="container">
                <!--Collapse content-->
		        <div class="collapse navbar-toggleable-xs" id="collapseEx2">
		            <!--Navbar Brand-->
		            <a href="index.php" class="navbar-brand" style="color:white;">Home</a>
		            <!--Links-->
		            <ul class="nav navbar-nav">
		                <li class="nav-item">
		                    <a class="nav-link">Jewellery</a>
		                </li>
		                <li class="nav-item">
		                    <a class="nav-link">Features</a>
		                </li>
		                <li class="nav-item">
		                    <a class="nav-link">Pricing</a>
		                </li>  
		            </ul>
                   
		            <form style="padding-top:8px; padding-left:12px;" class="form-inline float-lg-right">
                        <a href="" class="nav-link"><i class="fa fa-shopping-cart"  aria-hidden="true"></i>CART</a>
		            </form>
               
		            <form style="padding-top:8px; padding-left:12px; size:13px;" class="form-inline float-lg-right">
                        <a href="login.php" class="nav-link"><i class="fa fa-user" aria-hidden="true">Admin</i></a>
		            </form>

		            <!--Search form-->
		            <form style="padding-left:8px;" class="form-inline float-lg-right">
		                <input class="form-control" type="text" placeholder="Search">
		            </form>
		        </div>
		        <!--/.Collapse content-->
		    </div>
		</nav>
		<!--/.Navbar-->
		</br>
		</br>
		<!--Carousel Wrapper-->
        <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                <li data-target="#multi-item-example" data-slide-to="1"></li>
                <li data-target="#multi-item-example" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->

            <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <!--First slide-->
                <div class="carousel-item active" style="padding-left:150px; padding-right:150px;">

                    <div class="col-md-3">
                        <div class="card">
                            <img  class="img-fluid" src="I/1.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/2.jpg" alt="Jewellery">       
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/3.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/4.jpg" alt="Jewellery">
                        </div>
                    </div>

                </div>
                <!--/.First slide-->
                <!--Second slide-->
                <div class="carousel-item" style="padding-left:150px; padding-right:150px;">

                    <div class="col-md-3">
                        <div class="card">
                            <img class="img-fluid" src="I/5.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/6.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/7.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/8.jpg" alt="Jewellery">    
                        </div>
                    </div>
                </div>
                <!--/.Second slide-->
                <!--Third slide-->
                <div class="carousel-item" style="padding-left:150px; padding-right:150px;">

                    <div class="col-md-3">
                        <div class="card">
                            <img class="img-fluid" src="I/9.jpg" alt="Jewellery">  
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/10.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/11.jpg" alt="Jewellery">
                        </div>
                    </div>

                    <div class="col-md-3 hidden-sm-down">
                        <div class="card">
                            <img class="img-fluid" src="I/12.jpg" alt="Jewellery"> 
                        </div>
                    </div>
                </div>
                <!--/.Third slide-->
            </div>
            <!--/.Slides-->

        </div>
        <!--/.Carousel Wrapper-->

        <div class="container">
            <div class="row">
                <!--ist Rotating card-->
                <div class="col-md-3">
                <div class="card-wrapper" class="col-md-4" >
                    <div id="card-1" class="card-rotating effect__click">
                        <!--Front Side-->
                        <div class="face front">
                            <!-- Image-->
                            <div class="card-up">
                                <img src="I/d1.jpg" class="img-fluid">
                            </div>
                            <!--Avatar-->
                            <div class="avatar"><img src="I/d2.jpg" class="rounded-circle img-responsive">
                            </div>
                            <!--Content-->
                            <div class="card-block">
                                <h4>Damas Jewellery</h4>
                                <!--Triggering button-->
                                <a class="rotate-btn" data-card="card-1"><i class="fa fa-repeat"></i> Click here to rotate</a>
                            </div>
                        </div>
                        <!--/.Front Side-->

                        <!--Back Side-->
                        <div class="face back">
                            <!--Content-->
                            <h4>About me</h4>
                            <hr>
                            <p>Golden jewellery hand crafted by artisans in Pakistan, using semi precious stones.</p>
                            <hr>
                            <!--Social Icons-->
                            <ul class="inline-ul">
                                <li><a href="https://www.facebook.com" class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com" class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com" class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <!--Triggering button-->
                            <a class="rotate-btn" data-card="card-1"><i class="fa fa-undo"></i> Click here to rotate back</a>
                        </div>
                        <!--/.Back Side-->
                    </div>
                </div>
                </div>
                <!--/ist .Rotating card--> 
                <!--ist Rotating card-->
                <div class="col-md-3">
                <div class="card-wrapper" class="col-md-4">
                    <div id="card-2" class="card-rotating effect__click">
                        <!--Front Side-->
                        <div class="face front">
                            <!-- Image-->
                            <div class="card-up">
                                <img src="I/z1.jpg" class="img-fluid">
                            </div>
                            <!--Avatar-->
                            <div class="avatar"><img src="I/z2.jpg" class="rounded-circle img-responsive">
                            </div>
                            <!--Content-->
                            <div class="card-block">
                                <h4>Zevar</h4>
                                <!--Triggering button-->
                                <a class="rotate-btn" data-card="card-2"><i class="fa fa-repeat"></i> Click here to rotate</a>
                            </div>
                        </div>
                        <!--/.Front Side-->

                        <!--Back Side-->
                        <div class="face back">
                            <!--Content-->
                            <h4>About me</h4>
                            <hr>
                            <p>Our jewellery is inspired by the flora and fauna of Pakistan, and created using ancient inlay and metal working techniques.</p>
                            <hr>
                            <!--Social Icons-->
                            <ul class="inline-ul">
                                <li><a href="https://www.facebook.com" class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com" class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com" class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <!--Triggering button-->
                            <a class="rotate-btn" data-card="card-2"><i class="fa fa-undo"></i> Click here to rotate back</a>
                        </div>
                        <!--/.Back Side-->
                    </div>
                </div>  
                </div>
                <!--/ist .Rotating card--> 
                <!--Rotating card-->
                <div class="col-md-3">
                <div class="card-wrapper" class="col-md-4">
                    <div id="card-3" class="card-rotating effect__click">

                        <!--Front Side-->
                        <div class="face front">

                            <!-- Image-->
                            <div class="card-up">
                                <img src="I/p1.jpg" class="img-fluid">
                            </div>
                            <!--Avatar-->
                            <div class="avatar"><img src="I/p2.jpg" class="rounded-circle img-responsive">
                            </div>
                            <!--Content-->
                            <div class="card-block">
                                <h4>Piaget</h4>
                                <!--Triggering button-->
                                <a class="rotate-btn" data-card="card-3"><i class="fa fa-repeat"></i> Click here to rotate</a>
                            </div>
                        </div>
                        <!--/.Front Side-->

                        <!--Back Side-->
                        <div class="face back">

                            <!--Content-->
                            <h4>About me</h4>
                            <hr>
                            <p>Piaget has been creating luxury jewellery that are treasures of craftsmanship and refinement</p>
                            <hr>
                            <!--Social Icons-->
                            <ul class="inline-ul">
                                <li><a href="https://www.facebook.com" class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com" class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com" class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <!--Triggering button-->
                            <a class="rotate-btn" data-card="card-3"><i class="fa fa-undo"></i> Click here to rotate back</a>

                        </div>
                        <!--/.Back Side-->
                    </div>
                </div>
                </div>
                <!--/.Rotating card-->
                <!--Rotating card-->
                <div class="col-md-3">
                <div class="card-wrapper" class="col-md-4">
                    <div id="card-4" class="card-rotating effect__click">

                        <!--Front Side-->
                        <div class="face front">

                            <!-- Image-->
                            <div class="card-up">
                                <img src="I/h1.jpg" class="img-fluid">
                            </div>
                            <!--Avatar-->
                            <div class="avatar"><img src="I/h2.jpg" class="rounded-circle img-responsive">
                            </div>
                            <!--Content-->
                            <div class="card-block">
                                <h4>Hanif Jewellers</h4>
                                <!--Triggering button-->
                                <a class="rotate-btn" data-card="card-4"><i class="fa fa-repeat"></i> Click here to rotate</a>
                            </div>
                        </div>
                        <!--/.Front Side-->

                        <!--Back Side-->
                        <div class="face back">

                            <!--Content-->
                            <h4>About me</h4>
                            <hr>
                            <p>LThe Heritage of providing precious jewellery, Gold, Diamond, Gem Stone and pearl’s from the various part of the world.</p>
                            <hr>
                            <!--Social Icons-->
                            <ul class="inline-ul">
                                <li><a href="https://www.facebook.com" class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com" class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com" class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <!--Triggering button-->
                            <a class="rotate-btn" data-card="card-4"><i class="fa fa-undo"></i> Click here to rotate back</a>

                        </div>
                        <!--/.Back Side-->
                    </div>
                </div>
                </div>
                <!--/.Rotating card-->   
            </div>
        </div>

		<!--Footer-->
		<footer class="page-footer center-on-small-only">
		    <!--Footer Links-->
		    <div class="container-fluid">
		        <div class="row">
		            <!--First column-->
		            <div class="col-md-4 offset-md-1">
		                <h5 class="title">Online Jewellery Shop</h5>
		                <p>At Jeweller, we deal in best jewellery products.Since 2000, we have been helping our customers celebrate the milestones in their lives and we have done that by providing our customers with the finest quality jewelry at a good value.</p>
		            </div>
		            <!--/.First column-->

		            <hr class="hidden-md-up">

		            <!--Second column-->
		            <div class="col-md-2 offset-md-1">
		                <h5 class="title">Brands</h5>
		                <ul>
		                    <li><a href="piaget.php">Piaget</a></li>
		                    <li><a href="damajewel.php">Damas Jewellery</a></li>
		                    <li><a href="zevar.php">Zevar</a></li>
		                    <li><a href="hanifjewel.php">Hanif Jewellers</a></li>
		                </ul>
		            </div>
		            <!--/.Second column-->

		            <hr class="hidden-md-up">

		             <!--Third column-->
		            <div class="col-md-3">
		                <h5 class="title">Contact Details</h5>
		                <ul>
		                    <li>0345-2355367/051-4903532</li>
                            <li>Jewellery.com</li>
		                    <li>Islamabad, shop#56 F-10</li>
		                    <li>9:00 am to 5:00 pm</li>
		                </ul>
		            </div>
		            <!--/.Third column-->
		        </div>
		    </div>
		    <!--/.Footer Links-->

		    <hr>

		    <!--Call to action-->
		    <div class="call-to-action">
		        <ul>
		            <li><h5>Register for free</h5></li>
		            <li><a href="#" class="btn btn-primary">Sign up!</a></li>
		            <li>Already Registered?&nbsp;&nbsp;<a href="#" style="color:#809fff;"><b>Log in</b></a></li>
		        </ul>
		    </div>
		    <!--/.Call to action-->

		    <hr>

		    <!--Social buttons-->
		    <div class="social-section">
		        <ul>
		            <li><a href="https://www.facebook.com" class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://www.twitter.com" class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://plus.google.com" class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://www.linkedin.com" class="btn-floating btn-small btn-li"><i class="fa fa-linkedin"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://github.com" class="btn-floating btn-small btn-git"><i class="fa fa-github"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://www.pinterest.com" class="btn-floating btn-small btn-pin"><i class="fa fa-pinterest"> </i></a></li>&nbsp;&nbsp;
		            <li><a href="https://www.instagram.com" class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"> </i></a></li>
		        </ul>
		    </div>
		    <!--/.Social buttons-->

		    <!--Copyright-->
		    <div class="footer-copyright">
		        <div class="container-fluid">
		            © 2016 Copyright: <a href="http://www.MDBootstrap.com"><b> Jewellery.com </b></a>

		        </div>
		    </div>
		    <!--/.Copyright-->

		</footer>
		<!--/.Footer-->
        <!-- JQuery -->
        <script type="text/javascript" src="promdb/js/jquery3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="promdb/js/teether.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="promdb/js/boootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="promdb/js/mddb.min.js"></script>
	</body>
</html>	